// console.log('test')

// mock data = will contain all the posts
let posts = [];
// will be the id
let count = 1;

//
document.querySelector("#form-add-post").addEventListener('submit', e => {
    e.preventDefault()
    posts.push(
        {
            id: count,
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value
        }
    )

    // next available id (incremental value for id)
    count++
    
    showPosts(posts)
    alert("Successfully added ")

    document.querySelector("#txt-title").value = null
    document.querySelector("#txt-body").value = null
})

// Retrieve/Read (show posts)
const showPosts = posts => {

    let postEntries = ""

    posts.forEach(post => {
        
        postEntries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost('${post.id}')">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>
        `
    });

    console.log(postEntries)

    document.querySelector('#div-post-entries').innerHTML = postEntries
}

const editPost = id => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML
    
    document.querySelector('#txt-edit-id').value = id
    document.querySelector("#txt-edit-title").value = title
    document.querySelector("#txt-edit-body").value = body

    document.querySelector("#btn-submit-update").removeAttribute("disabled")

}

document.querySelector('#form-edit-post').addEventListener('submit', e => {

    e.preventDefault()
    let id = document.querySelector("#txt-edit-id").value

    posts[id-1].title = document.querySelector("#txt-edit-title").value
    posts[id-1].body = document.querySelector("#txt-edit-body").value
    // for(let i = 0; i < posts.length; i++){

    //     // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
    //     // Therefore, it is necesary to convert the Number to a String first.
    //     if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value ){

    //         // Reassign the property values
    //         posts[i].title = document.querySelector("#txt-edit-title").value
    //         posts[i].body = document.querySelector("#txt-edit-body").value

    showPosts(posts);
    alert("Successfully updated!")

    document.querySelector("#txt-edit-title").value = null
    document.querySelector("#txt-edit-body").value = null
    document.querySelector("#btn-submit-update").setAttribute("disabled", true)
    //     }
    })

function deletePost(id) {
    document.querySelector(`#post-${id}`).remove()
    console.log(posts.splice(posts.findIndex(post => post.id == id), 1))
}